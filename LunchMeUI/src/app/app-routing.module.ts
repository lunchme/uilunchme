import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompaniesComponent } from './company/companies/companies.component';
import { PostcompanyComponent } from './company/postcompany/postcompany.component';
import { CompanybyidComponent } from './company/showcompanybyid/companybyid.component';
import { UpdateComponent } from './company/updatecompany/update.component';
import { NotfoundErrorComponent } from './company/notfounderror/notfound-error.component';


const routes: Routes = [
  {path: 'companies' , component: CompaniesComponent },
  {path: 'create' , component: PostcompanyComponent },
  {path: 'company/:id', component: CompanybyidComponent },
  {path: 'updatecompany/:id', component: UpdateComponent },
  {path: 'displayError' , component: NotfoundErrorComponent },
  {path: '', redirectTo: 'companies', pathMatch: 'full'},
  {path: '**' , component: NotfoundErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
