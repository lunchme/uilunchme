import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CompaniesComponent } from './company/companies/companies.component';
import { HttpClientModule } from '@angular/common/http';
import { CompanybyidComponent } from './company/showcompanybyid/companybyid.component';
import { PostcompanyComponent } from './company/postcompany/postcompany.component';
import { UpdateComponent } from './company/updatecompany/update.component';
import { NotfoundErrorComponent } from './company/notfounderror/notfound-error.component';


@NgModule({
  declarations: [
    AppComponent,
    CompaniesComponent,
    CompanybyidComponent,
    PostcompanyComponent,
    UpdateComponent,
    NotfoundErrorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
