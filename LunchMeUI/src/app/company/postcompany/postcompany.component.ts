import { Component, OnInit } from '@angular/core';
import { Company } from '../company';
import { CompanyService } from '../company.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-postcompany',
  templateUrl: './postcompany.component.html',
  styleUrls: ['./postcompany.component.css']
})
export class PostcompanyComponent implements OnInit {
  CompanytoPost = new Company();

  constructor(private companyApi: CompanyService,
    private router: Router,
    private location: Location) {
  }

  postData(){
    this.companyApi.postCompany(this.CompanytoPost).subscribe((data) => {
      this.router.navigate(['/companies']);
    });
  }

  ngOnInit(): void {
  }

  goBack(): void {
    this.location.back();
  }
}
