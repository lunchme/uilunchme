import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { Company } from '../company';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-companybyid',
  templateUrl: './companybyid.component.html',
  styleUrls: ['./companybyid.component.css']
})
export class CompanybyidComponent implements OnInit {
  Company = new Company();

  constructor(private companyApi: CompanyService,
    private route: ActivatedRoute,
    private router: Router) { }

  deleteCompany(companyID: string): void{
    this.companyApi.deleteCompany(companyID).subscribe((data) => {
      console.log(companyID);
      this.router.navigate(['/companies']);
    });
  }

  onSelect(company){
    this.router.navigate(['/updatecompany', company.companyID]);
  }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    try {
      this.companyApi.getCompanyById(id).subscribe((data) => {
        /*  if ( data == null) {
            this.router.navigate(['/displayError']);
        }*/
        console.log(data);
        this.Company = data;
      });
    }
    catch (e) {
      this.router.navigate(['/displayError']);
    }
  }
}
