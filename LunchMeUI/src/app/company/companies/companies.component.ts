import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { Company } from '../company';
import { Router } from '@angular/router';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {
  data: Company[];

  constructor(private companyApi: CompanyService,
    private router: Router) { }

  onSelect(company) {
    this.router.navigate(['/company', company.companyID]);
  }

  ngOnInit(): void {
    this.companyApi.getAllCompanies().subscribe((data) => {
      this.data = data;
    });
  }
}
