import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { Company } from '../company';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  CompanyToUpdate = new Company();

  constructor(private companyApi: CompanyService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location) { }

  updateData(){
    this.companyApi.updateCompany(this.CompanyToUpdate.companyID, this.CompanyToUpdate).subscribe((data) => {
      this.router.navigate(['/companies']);
    });
  }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.companyApi.getCompanyById(id).subscribe((data) => {
      console.log(data);
      this.CompanyToUpdate = data;
    });
  }

  goBack(): void {
    this.location.back();
  }
}
