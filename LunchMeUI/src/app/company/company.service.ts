import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Company } from './company';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private url = 'https://localhost:44377/Company';
  constructor(private http: HttpClient) { }

  getAllCompanies(): Observable<any> {
    return this.http.get<any>(this.url);
  }

  getCompanyById(id: string): Observable<Company>{
    const url1 = `${this.url}/${id}`;

    return this.http.get<any>(url1);
  }

  postCompany(company: Company): Observable<Company>{
    return this.http.post<Company>(this.url, company);
  }
  deleteCompany(id: string): Observable<Company> {
    const url = `${this.url}?id=${id}`;
    return this.http.delete<Company>(url, this.httpOptions);
  }
  updateCompany(id: string, company: Company): Observable<any> {
    const url = `${this.url}?id=${id}`;
    return this.http.put(url, company, this.httpOptions);
  }
}
